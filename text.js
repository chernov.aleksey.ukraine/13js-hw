
let second = 1;
const generalProgress = () => {
    document.querySelector(`.btncont`).disabled = true;
    document.querySelector(`.btnstop`).disabled = false;
    const intervalId = setInterval(() => {
        if (second > 4) { second = 1 };
        document.querySelector(`.image-to-show`)?.classList.add("image-to-hide");
        document.querySelector(`.image-to-show`)?.classList.remove("image-to-show");
        document.querySelector(`.image${second}`)?.classList.remove("image-to-hide");
        document.querySelector(`.image${second}`)?.classList.add("image-to-show");
        console.log(second);
        second++;
    }, 3000);
    document.querySelector(`.btnstop`).addEventListener('click', () => setTimeout(() => {
        document.querySelector(`.btncont`).disabled = false;
        document.querySelector(`.btnstop`).disabled = true;
        clearInterval(intervalId);
    }, 0));
    document.querySelector(`.btncont`).addEventListener('click', generalProgress);
};
generalProgress();

